import React from 'react';
import { Route } from 'react-router-dom';
import './App.scss';
import TodoCreateEditPage from './Components/Pages/TodoCreateEditPage';
import TodosListPage from './Components/Pages/TodosListPage';

const App = () => {
  return (
    <>
      <Route exact component={TodosListPage} path="/" />
      <Route exact component={TodoCreateEditPage} path="/createTask" />
    </>
  );
};

export default App;
