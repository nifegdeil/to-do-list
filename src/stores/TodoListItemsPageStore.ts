import { makeAutoObservable } from 'mobx';
import DB from '../db/db.json';
export interface ITodoItem {
  id: number;
  title: string;
  completed: boolean;
  date: string;
  priority: number;
  description: string;
}

class TodoListItemsPage {
  todoItems: ITodoItem[] = [];
  filterType: 'all' | 'completed' | 'current' = 'all';
  constructor() {
    makeAutoObservable(this);
  }
  init() {
    this.getTodoItems();
  }
  getTodoItems() {
    this.todoItems = DB.todoListItems;
  }
  reset() {
    this.todoItems = [];
  }
  allTasksfilter = () => {
    this.filterType = 'all';
  };
  currentTasksfilter = () => {
    this.filterType = 'current';
  };
  completedTasksfilter = () => {
    this.filterType = 'completed';
  };
  addTodo(todo: ITodoItem) {
    this.todoItems.push(todo);
  }
  toggleCompleteTodo = (id: number) => {
    this.todoItems = this.todoItems.map((todo: ITodoItem) =>
      todo.id === id ? { ...todo, completed: !todo.completed } : todo
    );
  };
  get filteredList() {
    switch (this.filterType) {
      case 'all':
        return this.todoItems;
      case 'current':
        return this.todoItems.filter((todo) => todo.completed === false);
      case 'completed':
        return this.todoItems.filter((todo) => todo.completed === true);
    }
  }
}

export default new TodoListItemsPage();
