import * as React from 'react';
import './TodoItemCard.scss';

const TodoItemCard = ({ item, toggle }: any) => {
  return (
    <div className="todo-item">
      <div className="container">
        <div className="todo-item__content">
          <div className="todo-item__title">
            <h1>{item.title}</h1>
            <div className="todo-item__tabs">
              <div className="todo-item__priority">
                <p>Приоритет {item.priority}</p>
              </div>
              <div className="todo-item__date">
                <p>{item.date}</p>
              </div>
              <div className="todo-item__status">
                <p>{item.completed ? 'Выполнено' : 'Не выполнено'}</p>
              </div>
            </div>
          </div>
          <div className="todo-item__description">
            <p>{item.description}</p>
          </div>
          <div className="todo-item__buttons">
            <button className="edit-btn">Редактировать</button>
            <button className="add-btn" onClick={() => toggle(item.id)}>
              {item.completed ? 'Отменить выполнение' : 'Выполнить'}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default TodoItemCard;
