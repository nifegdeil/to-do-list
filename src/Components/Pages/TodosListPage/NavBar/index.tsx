import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.scss';

const NavBar = ({
  allTasksfilter,
  currentTasksfilter,
  completedTasksfilter,
}: any) => {
  return (
    <div className="navbar">
      <div className="container">
        <NavLink to="/createTask">
          <button>Добавить задачу</button>
        </NavLink>
        <div className="category">
          <p onClick={() => allTasksfilter()}>Все задачи</p>
          <p onClick={() => currentTasksfilter()}>Текущие задачи</p>
          <p onClick={() => completedTasksfilter()}>Завершённые</p>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
