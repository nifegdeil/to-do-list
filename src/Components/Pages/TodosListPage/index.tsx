import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';

import NavBar from './NavBar';
import TodoItemCard from './TodoItemCard';
import TodoListItemsPageStore, {
  ITodoItem,
} from '../../../stores/TodoListItemsPageStore';
import Layout from '../../Layout';

const TodosListPage = observer(() => {
  useEffect(() => {
    TodoListItemsPageStore.init();
  }, []);

  return (
    <Layout>
      <NavBar
        allTasksfilter={TodoListItemsPageStore.allTasksfilter}
        currentTasksfilter={TodoListItemsPageStore.currentTasksfilter}
        completedTasksfilter={TodoListItemsPageStore.completedTasksfilter}
      />
      <div>
        {TodoListItemsPageStore.filteredList.map((item: ITodoItem) => {
          return (
            <TodoItemCard
              toggle={TodoListItemsPageStore.toggleCompleteTodo}
              key={item.id}
              item={item}
            />
          );
        })}
      </div>
    </Layout>
  );
});

export default TodosListPage;
