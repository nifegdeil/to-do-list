import React from 'react';
import './Header.scss';

const Header = () => {
  return (
    <div className="header">
      <div className="container">
        <div className="header__logo">TODOAPP</div>
      </div>
    </div>
  );
};

export default Header;
