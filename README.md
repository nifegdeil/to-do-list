# Задачи

- Установить библиотеку react-router-dom версии 5+ не 6
- Создать три пустые страницы TodosListPage, TodoItemPage, TodoCreateEditPage
- Подключить страницы к роутингу
- Подключить MobX
